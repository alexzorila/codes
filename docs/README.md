# Code Snippets

This page is contains system code snippets.
The prefered format is one-liners. Some full scripts may be found in the "scripts" directory.

## Sample 1
This is the first sample of code
```
Code sample 1
```

## Sample 2
This is the second sample of code
```
Code sample 2
```

**EOF**